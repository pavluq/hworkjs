const newUser = {
    user: {
        firstName: '',
        lastName: '',
    },

    createNewUser(){
        this.firstName = prompt('Please, enter your first name');
        this.lastName = prompt('Please, enter your last name');
        return this;
    },

    getLogin(){
        return `${ this.firstName[0] }${ this.lastName }`.toLowerCase();
    },
}

const user = newUser.createNewUser();
console.log(user);
console.log(user.getLogin());
